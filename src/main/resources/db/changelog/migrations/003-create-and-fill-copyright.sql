CREATE TABLE copyright
(
    id           BIGSERIAL PRIMARY KEY,
    start_date   TIMESTAMP,
    end_date     TIMESTAMP,
    price        DECIMAL(10, 2),
    recording_id BIGINT REFERENCES recording (id),
    company_id   BIGINT REFERENCES company (id)
);

INSERT INTO copyright (start_date, end_date, price, recording_id, company_id) VALUES
    ('2022-01-02', '2028-12-31', 10.00, 1, 1);

INSERT INTO copyright (start_date, end_date, price, recording_id, company_id) VALUES
    ('2021-01-01', '2023-12-31', 12.50, 2, 2);
