package com.example.offerdaysongs.controller;

import com.example.offerdaysongs.dto.CompanyDto;
import com.example.offerdaysongs.dto.CopyrightDto;
import com.example.offerdaysongs.dto.RecordingDto;
import com.example.offerdaysongs.dto.SingerDto;
import com.example.offerdaysongs.dto.requests.CreateCopyrightRequest;
import com.example.offerdaysongs.dto.requests.UpdateCopyrightRequest;
import com.example.offerdaysongs.model.Copyright;
import com.example.offerdaysongs.service.CopyrightService;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/copyrights")
public class CopyrightController {
    private static final String ID = "id";
    private final CopyrightService copyrightService;

    public CopyrightController(CopyrightService copyrightService) {
        this.copyrightService = copyrightService;
    }

    @GetMapping("/")
    public List<CopyrightDto> getAll() {
        return copyrightService.getAll().stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id:[\\d]+}")
    public CopyrightDto get(@PathVariable(ID) long id) {
        var company = copyrightService.getById(id);
        return convertToDto(company);
    }

    @PostMapping("/")
    public CopyrightDto create(@RequestBody CreateCopyrightRequest request) {
        return convertToDto(copyrightService.create(request));
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable long id) {
        copyrightService.delete(id);
    }

    @PutMapping()
    public CopyrightDto update(@RequestBody UpdateCopyrightRequest request) {
        return convertToDto(copyrightService.update(request));
    }

    @GetMapping("/getByCompany/{id:[\\d]+}")
    public List<CopyrightDto> getByCompany(@PathVariable long id) {
        return copyrightService.getByCompanyId(id).stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }

    @GetMapping("/getByPeriod")
    public List<CopyrightDto> getByPeriod(@RequestParam(value = "startDate") String startDate,
                                          @RequestParam(value = "endDate") String endDate) {
        return copyrightService.getCopyrightByPeriod(startDate, endDate).stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }

    private CopyrightDto convertToDto(Copyright copyright) {
        var recording = copyright.getRecording();
        var company = copyright.getCompany();
        return new CopyrightDto(copyright.getId(),
                copyright.getStartDate(),
                copyright.getEndDate(),
                copyright.getPrice(),
                company != null ? new CompanyDto(company.getId(), company.getName()) : null,
                recording != null ? new RecordingDto(recording.getId(), recording.getTitle(),
                        recording.getVersion(), recording.getReleaseTime(),
                        recording.getSinger() != null ? new SingerDto(recording.getSinger().getId(),
                                recording.getSinger().getName()) : null) : null
        );
    }
}