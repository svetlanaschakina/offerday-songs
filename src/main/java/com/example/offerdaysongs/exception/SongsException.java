package com.example.offerdaysongs.exception;

public class SongsException extends RuntimeException {
    public SongsException(String message) {
        super(message);
    }
}