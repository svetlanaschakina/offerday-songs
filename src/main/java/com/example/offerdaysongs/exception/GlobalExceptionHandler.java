package com.example.offerdaysongs.exception;

import com.example.offerdaysongs.dto.responces.BadResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(SongsException.class)
    public ResponseEntity<BadResponse> handleSongsException(SongsException exception) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new BadResponse(exception.getMessage()));
    }
}