package com.example.offerdaysongs.service;

import com.example.offerdaysongs.dto.requests.CreateCopyrightRequest;
import com.example.offerdaysongs.dto.requests.UpdateCopyrightRequest;
import com.example.offerdaysongs.exception.SongsException;
import com.example.offerdaysongs.model.Company;
import com.example.offerdaysongs.model.Copyright;
import com.example.offerdaysongs.model.Recording;
import com.example.offerdaysongs.model.Singer;
import com.example.offerdaysongs.repository.CompanyRepository;
import com.example.offerdaysongs.repository.CopyrightRepository;
import com.example.offerdaysongs.repository.RecordingRepository;
import com.example.offerdaysongs.repository.SingerRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class CopyrightService {
    private final CopyrightRepository copyrightRepository;
    private final CompanyRepository companyRepository;
    private final RecordingRepository recordingRepository;
    private final SingerRepository singerRepository;

    public CopyrightService(CopyrightRepository copyrightRepository,
                            CompanyRepository companyRepository,
                            RecordingRepository recordingRepository,
                            SingerRepository singerRepository) {
        this.copyrightRepository = copyrightRepository;
        this.companyRepository = companyRepository;
        this.recordingRepository = recordingRepository;
        this.singerRepository = singerRepository;
    }

    public List<Copyright> getAll() {
        return copyrightRepository.findAll();
    }

    public Copyright getById(long id) {
        return copyrightRepository.getById(id);
    }

    @Transactional
    public Copyright create(CreateCopyrightRequest request) {
        Copyright copyright = new Copyright();
        copyright.setStartDate(request.getStartDate());
        copyright.setEndDate(request.getEndDate());
        copyright.setPrice(request.getPrice());
        var companyDto = request.getCompany();
        if (companyDto != null) {
            var company = companyRepository.findById(companyDto.getId()).orElseGet(() -> {
                var temp = new Company();
                temp.setName(companyDto.getName());
                return companyRepository.save(temp);
            });
            copyright.setCompany(company);
        }
        var recordingDto = request.getRecording();
        if (recordingDto != null) {
            var recording = recordingRepository.findById(recordingDto.getId()).orElseGet(() -> {
                var temp = new Recording();
                temp.setTitle(recordingDto.getTitle());
                temp.setReleaseTime(recordingDto.getReleaseTime());
                temp.setVersion(recordingDto.getVersion());
                var singerDto = recordingDto.getSinger();
                if (singerDto != null) {
                    var singer = singerRepository.findById(singerDto.getId()).orElseGet(() -> {
                        var tempSinger = new Singer();
                        tempSinger.setName(singerDto.getName());
                        return singerRepository.save(tempSinger);
                    });
                    temp.setSinger(singer);
                }
                return recordingRepository.save(temp);
            });
            copyright.setRecording(recording);
        }
        return copyrightRepository.save(copyright);
    }

    public void delete(long id) {
        Copyright copyright = copyrightRepository.findById(id)
                .orElseThrow(() -> new SongsException("Право с id " + id + " не найдено"));
        copyrightRepository.delete(copyright);
    }

    public Copyright update(UpdateCopyrightRequest request) {
        Copyright copyright = copyrightRepository.findById(request.getId())
                .orElseThrow(() -> new SongsException("Право не найдено"));
        copyright.setStartDate(request.getStartDate());
        copyright.setEndDate(request.getEndDate());
        copyright.setPrice(request.getPrice());
        copyright.setCompany(request.getCompany());
        copyright.setRecording(request.getRecording());
        return copyrightRepository.save(copyright);
    }

    public List<Copyright> getByCompanyId(long id) {
        return copyrightRepository.findAllByCompanyId(id);
    }

    public List<Copyright> getCopyrightByPeriod(String startDate, String endDate) {
        return copyrightRepository.findByPeriod(startDate, endDate);
    }
}