package com.example.offerdaysongs.repository;

import com.example.offerdaysongs.model.Copyright;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CopyrightRepository extends JpaRepository<Copyright, Long>, JpaSpecificationExecutor<Copyright> {
    List<Copyright> findAllByCompanyId(long id);

    @Query(value = "SELECT * FROM copyright " +
                   "WHERE ((?1 BETWEEN start_date AND end_date) AND (?2 BETWEEN start_date AND end_date))",
                    nativeQuery = true)
    List<Copyright> findByPeriod(String startDate, String endDate);
}
