package com.example.offerdaysongs.dto.requests;

import com.example.offerdaysongs.model.Company;
import com.example.offerdaysongs.model.Recording;
import lombok.Data;

import java.time.ZonedDateTime;

@Data
public class UpdateCopyrightRequest {
    private long id;
    private ZonedDateTime startDate;
    private ZonedDateTime endDate;
    private double price;
    private Recording recording;
    private Company company;
}
